﻿using System;
using Microsoft.Extensions.Configuration;

namespace Car1.Flujo.Compra.Api.Util
{
    public class Constants
    {
        public const string AllowOrigins = "_OriginPolicy";
        public static class AppSettings
        {
#if DEBUG
            public static string BaseArticulo = GetSetting<string>("api", "Articulo", "base");
#else
            public static string BaseArticulo = GetSetting<string>("api", "Articulo", "baseProd");
#endif
            public static string AgregarArticulo = GetSetting<string>("api", "Articulo", "agregar");
#if DEBUG
            public static string ConnectionString = GetSetting<string>("mysqlconnection", "connectionString");
#else
            public static string ConnectionString = GetSetting<string>("mysqlconnection", "connecStringProd");
#endif

            private static IConfigurationRoot _config = null;
            private static void InitConfig() =>
                _config = new ConfigurationBuilder()
                    .AddJsonFile("appsettings.json")
                    .Build();
            private static T GetSetting<T>(params string[] jsonPath)
            {
                if (_config == null)
                    InitConfig();

                return (T)Convert.ChangeType(_config[$"{string.Join(':', jsonPath)}"], typeof(T));
            }
        }
    }
}
