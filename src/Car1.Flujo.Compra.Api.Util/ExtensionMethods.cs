﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Car1.Flujo.Compra.Api.Util
{
    public static class ExtensionMethods
    {
        public static long FormatTelefono(this string telefono)
        {
            if (string.IsNullOrWhiteSpace(telefono)) return 0;
            telefono = telefono.Replace(" ", string.Empty).Replace("+", string.Empty);

            return long.TryParse(telefono, out long fono) ? fono : 0;
        }
        public static int ParseInt(this string texto)
        {
            return int.TryParse(texto, out int numero) ? numero : 0;
        }
        public static int? ParseNullInt(this string texto)
        {
            return int.TryParse(texto, out int numero) ? numero : (int?)null;
        }
        public static int[] ArrayParse(this string texto, char separador)
        {
            if (string.IsNullOrWhiteSpace(texto)) return new int[0];
            return texto.Split(separador).Select(int.Parse).ToArray();
        }
    }
}
