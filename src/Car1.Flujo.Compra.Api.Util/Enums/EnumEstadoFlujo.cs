﻿namespace Car1.Flujo.Compra.Api.Util.Enums
{
    public enum EnumEstadoFlujo
    {
        Ninguno = 0,
        EnEmision = 1,
        Cancelado,
        EnProcesoPago,
        Finalizado
    }
}
