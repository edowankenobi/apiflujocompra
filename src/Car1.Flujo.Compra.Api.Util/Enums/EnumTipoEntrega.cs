﻿namespace Car1.Flujo.Compra.Api.Util.Enums
{
    public enum EnumTipoEntrega
    {
        DespachoRM = 1,
        DespachoRegiones,
        RetiroLocal
    }
}
