﻿using Car1.Flujo.Compra.Api.Business;
using Car1.Flujo.Compra.Api.Model;
using Car1.Flujo.Compra.Api.Model.FlujoCompra;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Car1.Flujo.Compra.Api.Win.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FlujoController : ControllerBase
    {
        // GET api/flujo/obtener/5
        [HttpGet]
        [Route("obtener/{id}")]
        public IActionResult Get(int id)
        {
            return Ok(new FlujoCompraBO().Obtener(id));
        }
        [HttpGet]
        [Route("obtener-emision/{id}")]
        public IActionResult ObtenerEmision(int id)
        {
            return Ok(new FlujoCompraBO().Obtener(id, Util.Enums.EnumEstadoFlujo.EnEmision));
        }
        [HttpPost]
        [Route("obtener")]
        public IActionResult ObtenerTodosPerId(IdModel<int> value)
        {
            return Ok(new FlujoCompraBO().ObtenerTodosPorPerId(value));
        }
        // POST api/flujo
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreaFlujoCompraRequestModel value)
        {
            try
            {
                return Ok(await new FlujoCompraBO().Crear(value));
            }
            catch (Exception)
            {
                return BadRequest(0);
            }
        }
        [HttpPost]
        [Route("finalizar")]
        public async Task<IActionResult> Finalizar([FromBody] IdModel<int> value)
        {
            try
            {
                return Ok(await new FlujoCompraBO().Finalizar(value));
            }
            catch (Exception)
            {
                return BadRequest(0);
            }
        }
        [HttpPost]
        [Route("anular")]
        public async Task<IActionResult> Anular([FromBody] IdModel<int> value)
        {
            try
            {
                return Ok(await new FlujoCompraBO().Finalizar(value));
            }
            catch (Exception)
            {
                return BadRequest(0);
            }
        }
        [HttpPost]
        [Route("financiamiento/{flujoId}/{acepta}")]
        public IActionResult AceptaRechazaFinanciamiento(int flujoId, bool acepta)
        {
            try
            {
                return Ok(new FlujoCompraBO().Financiamiento(acepta, flujoId));
            }
            catch (Exception)
            {
                return BadRequest(false);
            }
        }
        [HttpGet]
        [Route("Aceptafinanciamiento/{flujoId}")]
        public IActionResult AceptaFinanciamiento(int flujoId)
        {
            try
            {
                return Ok(new FlujoCompraBO().Financiamiento(flujoId));
            }
            catch (Exception)
            {
                return BadRequest(false);
            }
        }
    }
}
