﻿using Car1.Flujo.Compra.Api.Business;
using Car1.Flujo.Compra.Api.Model.Entrega;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Car1.Flujo.Compra.Api.Win.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EntregaController : ControllerBase
    {

        [HttpPost]
        [Route("metropolitana")]
        public async Task<IActionResult> Post([FromBody] DespachoRmModel value)
        {
            if (ModelState.IsValid)
                return Ok(await new EntregaBO().GuardaRm(value));
            return BadRequest(false);
        }
    }
}
