﻿using Car1.Flujo.Compra.Api.Entity.FlujoCompra;
using Car1.Flujo.Compra.Api.Infrastructure.Data;
using Car1.Flujo.Compra.Api.Model.Entrega;
using Car1.Flujo.Compra.Api.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Car1.Flujo.Compra.Api.Business
{
    public class EntregaBO
    {
        public async Task<bool> GuardaRm(DespachoRmModel despachoRm)
        {
            try
            {
                using var dbcontext = new FlujoCompraContext();
                var flujo = dbcontext.TFlujoCompras.FirstOrDefault(c => c.FlujoCompraId == despachoRm.FlujoId);

                if (flujo == null) return false;
                var entregaExistente = dbcontext.TEntregas.FirstOrDefault(c => c.EntregaId == flujo.EntregaId);
                if (entregaExistente != null)
                    dbcontext.TEntregas.Remove(entregaExistente);

                var entrega = new TEntrega()
                {
                    Nombres = despachoRm.Nombres,
                    PrimerApellido = despachoRm.PrimerApellido,
                    SegundoApellido = despachoRm.SegundoApellido,
                    Rut = despachoRm.Rut.ToUpper(),
                    RegionId = 7,
                    ComunaId = despachoRm.Comuna,
                    Telefono = despachoRm.Telefono.FormatTelefono(),
                    Calle = despachoRm.Calle,
                    Depto = despachoRm.Depto.ParseNullInt(),
                    Numero = despachoRm.Numero.ParseNullInt(),
                    FechaCreacion = DateTime.Now,
                    TipoEntregaId = (int)Util.Enums.EnumTipoEntrega.DespachoRM,
                    Vigente = true
                };
                await dbcontext.TEntregas.AddAsync(entrega);
                bool guarda = await dbcontext.SaveChangesAsync() > 0;

                flujo.EntregaId = entrega.EntregaId;
                dbcontext.TFlujoCompras.Update(flujo);

                return guarda && await dbcontext.SaveChangesAsync() > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
