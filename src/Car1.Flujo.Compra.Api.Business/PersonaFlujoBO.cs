﻿using Car1.Flujo.Compra.Api.Infrastructure.Data;
using Car1.Flujo.Compra.Api.Model.FlujoCompra;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Car1.Flujo.Compra.Api.Model;
using Microsoft.EntityFrameworkCore;
using Car1.Flujo.Compra.Api.Model.PersonaFlujo;

namespace Car1.Flujo.Compra.Api.Business
{
    public class PersonaFlujoBO
    {
        public List<IdValue<int, string>> ObtieneListPersonasFlujo(int flujoId)
        {
            var retorno = new List<IdValue<int, string>>();

            using var dbcontext = new FlujoCompraContext();
            var flujo = dbcontext.TFlujoCompras.Include(c => c.PerTransferencia).Include(c => c.Persona).FirstOrDefault(c => c.FlujoCompraId.Equals(flujoId) && c.Vigente);

            if (flujo.Persona != null && !flujo.Persona.EsEmpresa)
                retorno.Add(new IdValue<int, string>(flujo.Persona.PersonaFlujoId, $"{flujo.Persona.Nombres} {flujo.Persona.PrimerApellido}"));

            if (flujo.PerTransferencia != null)
                retorno.Add(new IdValue<int, string>(flujo.PerTransferencia.PersonaFlujoId, $"{flujo.PerTransferencia.Nombres} {flujo.PerTransferencia.PrimerApellido}"));

            return retorno;
        }
        public PersonaModelResponse ObtienePersonaFlujo(int perFlujoId)
        {
            using var dbcontext = new FlujoCompraContext();
            var persona = dbcontext.TPersonaFlujos.FirstOrDefault(c => c.PersonaFlujoId == perFlujoId);

            if (persona != null)
                return new PersonaModelResponse
                {
                    Calle = persona.Calle,
                    Comuna = persona.ComunaId,
                    Depto = persona.Depto?.ToString(),
                    Email = persona.Email,
                    Nombres = persona.Nombres,
                    PrimerApellido = persona.PrimerApellido,
                    SegundoApellido = persona.SegundoApellido,
                    Numero = persona.Numero.ToString(),
                    Region = persona.RegionId,
                    Rut = persona.Rut,
                    Telefono = persona.Telefono.ToString()
                };
            return null;
        }
    }
}
