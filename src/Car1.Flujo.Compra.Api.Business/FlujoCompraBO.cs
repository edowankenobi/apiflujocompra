﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Car1.Flujo.Compra.Api.Entity.FlujoCompra;
using Car1.Flujo.Compra.Api.Infrastructure.Data;
using Car1.Flujo.Compra.Api.Model;
using Car1.Flujo.Compra.Api.Model.FlujoCompra;
using Car1.Flujo.Compra.Api.Services;
using Car1.Flujo.Compra.Api.Util.Enums;

namespace Car1.Flujo.Compra.Api.Business
{
    public class FlujoCompraBO
    {
        public async Task<int> Crear(CreaFlujoCompraRequestModel flujoCompra)
        {
            var articulo = await new ArticuloService().CrearArticulo(flujoCompra.Patente);
            if (articulo == null || articulo.ArticuloId <= 0) throw new ApplicationException("Error de generación");

            var flujo = new TFlujoCompra()
            {
                ArticuloId = articulo.ArticuloId,
                SessionId = flujoCompra.SessionId,
                ValorArticulo = articulo.Precio,
                TransferenciaArticulo = articulo.ValorTransferencia,
                ValorEntrega = 0,
                FechaCreacion = DateTime.Now,
                EstadoFlujoId = (int)EnumEstadoFlujo.EnEmision,
                Vigente = true
            };
            using var dbcontext = new FlujoCompraContext();
            await dbcontext.TFlujoCompras.AddAsync(flujo);
            return await dbcontext.SaveChangesAsync() > 0 ? flujo.FlujoCompraId : 0;
        }
        public async Task<bool> Finalizar(IdModel<int> flujoId)
        {
            using var dbcontext = new FlujoCompraContext();
            var flujo = dbcontext.TFlujoCompras.FirstOrDefault(c => c.FlujoCompraId == flujoId.Value);

            flujo.EstadoFlujoId = (int)EnumEstadoFlujo.EnProcesoPago;
            dbcontext.TFlujoCompras.Update(flujo);

            return await dbcontext.SaveChangesAsync() > 0;
        }
        public async Task<bool> Anular(IdModel<int> flujoId)
        {
            using var dbcontext = new FlujoCompraContext();
            var flujo = dbcontext.TFlujoCompras.FirstOrDefault(c => c.FlujoCompraId == flujoId.Value);

            flujo.EstadoFlujoId = (int)EnumEstadoFlujo.Cancelado;
            dbcontext.TFlujoCompras.Update(flujo);

            return await dbcontext.SaveChangesAsync() > 0;
        }
        public FlujoCompraResponseModel Obtener(int flujoCompraId, EnumEstadoFlujo estadoFlujo = EnumEstadoFlujo.Ninguno)
        {
            using var dbcontext = new FlujoCompraContext();
            var flujo = 
                estadoFlujo == EnumEstadoFlujo.Ninguno ?
                dbcontext.TFlujoCompras.FirstOrDefault(c => c.FlujoCompraId == flujoCompraId) :
                dbcontext.TFlujoCompras.FirstOrDefault(c => c.FlujoCompraId == flujoCompraId && c.EstadoFlujoId == (int)estadoFlujo);

            if (flujo == null) return null;

            var respuesta = new FlujoCompraResponseModel()
            {
                ArticuloId = flujo.ArticuloId,
                FlujoCompraId = flujo.FlujoCompraId,
                PersonaId = flujo.PersonaId,
                PerTransferenciaId = flujo.PerTransferenciaId,
                TransferenciaArticulo = flujo.TransferenciaArticulo,
                ValorArticulo = flujo.ValorArticulo,
                ValorEntrega = flujo.ValorEntrega,
                FinPersonaId = flujo.FinPersonaId
            };

            return respuesta;
        }
        public List<FlujoCompraResponseModel> ObtenerTodosPorPerId(IdModel<int> perId)
        {
            using var dbcontext = new FlujoCompraContext();

            var estados = new int[] { (int)EnumEstadoFlujo.EnProcesoPago, (int)EnumEstadoFlujo.Finalizado };
            var flujos = dbcontext.TFlujoCompras.Where(c => c.FinPersonaId == perId.Value && estados.Contains(c.EstadoFlujoId));

            if (flujos == null || !flujos.Any()) return null;

            return flujos.Select(flujo => new FlujoCompraResponseModel()
            {
                ArticuloId = flujo.ArticuloId,
                FlujoCompraId = flujo.FlujoCompraId,
                PersonaId = flujo.PersonaId,
                PerTransferenciaId = flujo.PerTransferenciaId,
                TransferenciaArticulo = flujo.TransferenciaArticulo,
                ValorArticulo = flujo.ValorArticulo,
                ValorEntrega = flujo.ValorEntrega,
                FinPersonaId = flujo.FinPersonaId
            }).ToList();
        }

        public bool Financiamiento(bool acepta, int flujoId)
        {
            using var dbcontext = new FlujoCompraContext();

            var flujo = dbcontext.TFlujoCompras.FirstOrDefault(c => c.FlujoCompraId == flujoId);

            if (flujo == null) return false;

            flujo.AceptaFinanciamiento = acepta;

            return dbcontext.SaveChanges() > 0;
        }
        public bool Financiamiento(int flujoId)
        {
            using var dbcontext = new FlujoCompraContext();

            var flujo = dbcontext.TFlujoCompras.FirstOrDefault(c => c.FlujoCompraId == flujoId);

            return (flujo?.AceptaFinanciamiento ?? false);
        }
    }
}
