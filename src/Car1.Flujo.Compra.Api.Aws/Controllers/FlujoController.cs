using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Car1.Flujo.Compra.Api.Business;
using Car1.Flujo.Compra.Api.Model.FlujoCompra;
using Microsoft.AspNetCore.Mvc;

namespace Car1.Flujo.Compra.Api.Aws.Controllers
{
    [Route("api/[controller]")]
    public class FlujoController : ControllerBase
    {
        // GET api/flujo/5
        [HttpGet]
        [Route("ObtenerFlujo")]
        public IActionResult Get(int id)
        {
            return Ok(new FlujoCompraBO().Obtener(id));
        }

        // POST api/flujo
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreaFlujoCompraRequestModel value)
        {
            try
            {
                return Ok(await new FlujoCompraBO().Crear(value));
            }
            catch (Exception)
            {
                return BadRequest(0);
            }
        }
    }
}
