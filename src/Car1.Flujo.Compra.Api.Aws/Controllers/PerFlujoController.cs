﻿using Car1.Flujo.Compra.Api.Business;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Car1.Flujo.Compra.Api.Aws.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PerFlujoController : ControllerBase
    {
        //flujoId
        [Route("list")]
        [HttpGet]
        public IActionResult List(int id)
        {
            return Ok(new PersonaFlujoBO().ObtieneListPersonasFlujo(id));
        }
        [HttpGet]
        public IActionResult Get(int id)
        {
            try
            {
                var persona = new PersonaFlujoBO().ObtienePersonaFlujo(id);
                if (persona != null)
                    return Ok(persona);
                return BadRequest(new { });
            }
            catch (Exception)
            {
                return BadRequest(new { });
            }
        }
    }
}
