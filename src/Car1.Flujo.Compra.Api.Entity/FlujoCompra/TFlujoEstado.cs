﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Car1.Flujo.Compra.Api.Entity.FlujoCompra
{
    public partial class TFlujoEstado
    {
        public TFlujoEstado()
        {
            TFlujoCompras = new HashSet<TFlujoCompra>();
        }

        public int EstadoId { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<TFlujoCompra> TFlujoCompras { get; set; }
    }
}
