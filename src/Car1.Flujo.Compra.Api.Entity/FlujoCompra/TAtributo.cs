﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Car1.Flujo.Compra.Api.Entity.FlujoCompra
{
    public partial class TAtributo
    {
        public TAtributo()
        {
            TArticuloAtributos = new HashSet<TArticuloAtributo>();
        }

        public int AtributoId { get; set; }
        public string Atributo { get; set; }
        public int TipoDatoId { get; set; }
        public bool Vigente { get; set; }

        public virtual TTipoDato TipoDato { get; set; }
        public virtual ICollection<TArticuloAtributo> TArticuloAtributos { get; set; }
    }
}
