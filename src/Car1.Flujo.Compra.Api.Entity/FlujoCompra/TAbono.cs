﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Car1.Flujo.Compra.Api.Entity.FlujoCompra
{
    public partial class TAbono
    {
        public int AbonoId { get; set; }
        public int FlujoCompraId { get; set; }
        public int MontoAbono { get; set; }
        public DateTime FechaAbono { get; set; }
        public DateTime FechaAbonoUtc { get; set; }
        public int AbonoEstadoId { get; set; }
        public string OrdenKlap { get; set; }

        public virtual TAbonoEstado AbonoEstado { get; set; }
    }
}
