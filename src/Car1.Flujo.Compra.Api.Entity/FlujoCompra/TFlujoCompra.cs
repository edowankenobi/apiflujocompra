﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Car1.Flujo.Compra.Api.Entity.FlujoCompra
{
    public partial class TFlujoCompra
    {
        public TFlujoCompra()
        {
            TFlujoTiempos = new HashSet<TFlujoTiempo>();
        }

        public int FlujoCompraId { get; set; }
        public int ArticuloId { get; set; }
        public int? PersonaId { get; set; }
        public int? PerTransferenciaId { get; set; }
        public int? EntregaId { get; set; }
        public int EstadoFlujoId { get; set; }
        public int? FinPersonaId { get; set; }
        public bool? AceptaFinanciamiento { get; set; }
        public decimal ValorArticulo { get; set; }
        public decimal TransferenciaArticulo { get; set; }
        public decimal ValorEntrega { get; set; }
        public DateTime FechaCreacion { get; set; }
        public bool Vigente { get; set; }
        public string SessionId { get; set; }

        public virtual TArticulo Articulo { get; set; }
        public virtual TFlujoEstado EstadoFlujo { get; set; }
        public virtual TPersonaFlujo PerTransferencia { get; set; }
        public virtual TPersonaFlujo Persona { get; set; }
        public virtual ICollection<TFlujoTiempo> TFlujoTiempos { get; set; }
    }
}
