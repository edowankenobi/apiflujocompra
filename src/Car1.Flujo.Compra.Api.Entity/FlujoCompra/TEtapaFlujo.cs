﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Car1.Flujo.Compra.Api.Entity.FlujoCompra
{
    public partial class TEtapaFlujo
    {
        public int EtapaId { get; set; }
        public string Etapa { get; set; }
        public bool Vigente { get; set; }
    }
}
