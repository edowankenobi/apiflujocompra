﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Car1.Flujo.Compra.Api.Entity.FlujoCompra
{
    public partial class TTipoDato
    {
        public TTipoDato()
        {
            TAtributos = new HashSet<TAtributo>();
        }

        public int TipoDatoId { get; set; }
        public string TipoDato { get; set; }
        public DateTime FechaCreacion { get; set; }
        public bool Vigente { get; set; }

        public virtual ICollection<TAtributo> TAtributos { get; set; }
    }
}
