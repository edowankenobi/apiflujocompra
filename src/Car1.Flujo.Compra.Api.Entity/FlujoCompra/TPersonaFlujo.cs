﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Car1.Flujo.Compra.Api.Entity.FlujoCompra
{
    public partial class TPersonaFlujo
    {
        public TPersonaFlujo()
        {
            TFlujoCompraPerTransferencia = new HashSet<TFlujoCompra>();
            TFlujoCompraPersonas = new HashSet<TFlujoCompra>();
        }

        public int PersonaFlujoId { get; set; }
        public string Rut { get; set; }
        public string Nombres { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public long Telefono { get; set; }
        public string Email { get; set; }
        public int RegionId { get; set; }
        public int ComunaId { get; set; }
        public string Calle { get; set; }
        public int Numero { get; set; }
        public int? Depto { get; set; }
        public bool EsEmpresa { get; set; }
        public string RazonSocial { get; set; }
        public string Giro { get; set; }

        public virtual ICollection<TFlujoCompra> TFlujoCompraPerTransferencia { get; set; }
        public virtual ICollection<TFlujoCompra> TFlujoCompraPersonas { get; set; }
    }
}
