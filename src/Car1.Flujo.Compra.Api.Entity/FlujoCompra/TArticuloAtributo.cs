﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Car1.Flujo.Compra.Api.Entity.FlujoCompra
{
    public partial class TArticuloAtributo
    {
        public int ArticuloAtributoId { get; set; }
        public int ArticuloId { get; set; }
        public int AtributoId { get; set; }
        public string Valor { get; set; }
        public bool Vigente { get; set; }

        public virtual TArticulo Articulo { get; set; }
        public virtual TAtributo Atributo { get; set; }
    }
}
