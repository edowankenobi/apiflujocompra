﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Car1.Flujo.Compra.Api.Entity.FlujoCompra
{
    public partial class TArticulo
    {
        public TArticulo()
        {
            TArticuloAtributos = new HashSet<TArticuloAtributo>();
        }

        public int ArticuloId { get; set; }
        public int ArticuloTipoId { get; set; }
        public string Nombre { get; set; }
        public string IdentificadorExterno { get; set; }
        public long Precio { get; set; }
        public int ValorTransferencia { get; set; }
        public DateTime FechaCreacion { get; set; }
        public bool Vigente { get; set; }

        public virtual TArticuloTipo ArticuloTipo { get; set; }
        public virtual ICollection<TArticuloAtributo> TArticuloAtributos { get; set; }
    }
}
