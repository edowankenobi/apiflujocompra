﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Car1.Flujo.Compra.Api.Entity.FlujoCompra
{
    public partial class TFlujoTiempo
    {
        public int TiempoId { get; set; }
        public int FlujoCompraId { get; set; }
        public DateTime FechaFin { get; set; }
        public DateTime FechaCreacion { get; set; }
        public bool Vigente { get; set; }

        public virtual TFlujoCompra FlujoCompra { get; set; }
    }
}
