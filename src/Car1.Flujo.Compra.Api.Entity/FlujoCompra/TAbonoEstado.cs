﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Car1.Flujo.Compra.Api.Entity.FlujoCompra
{
    public partial class TAbonoEstado
    {
        public TAbonoEstado()
        {
            TAbonos = new HashSet<TAbono>();
        }

        public int AbonoEstadoId { get; set; }
        public string Estado { get; set; }
        public bool Vigente { get; set; }

        public virtual ICollection<TAbono> TAbonos { get; set; }
    }
}
