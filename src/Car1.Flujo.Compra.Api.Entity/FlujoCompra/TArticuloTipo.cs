﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Car1.Flujo.Compra.Api.Entity.FlujoCompra
{
    public partial class TArticuloTipo
    {
        public TArticuloTipo()
        {
            TArticulos = new HashSet<TArticulo>();
        }

        public int ArticuloTipoId { get; set; }
        public string Tipo { get; set; }
        public string Descripcion { get; set; }
        public bool Vigente { get; set; }

        public virtual ICollection<TArticulo> TArticulos { get; set; }
    }
}
