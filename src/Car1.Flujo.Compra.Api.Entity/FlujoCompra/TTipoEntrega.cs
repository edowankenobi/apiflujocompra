﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Car1.Flujo.Compra.Api.Entity.FlujoCompra
{
    public partial class TTipoEntrega
    {
        public int TipoEntregaId { get; set; }
        public string Descripcion { get; set; }
    }
}
