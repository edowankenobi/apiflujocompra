﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Car1.Flujo.Compra.Api.Entity.FlujoCompra
{
    public partial class TEntrega
    {
        public int EntregaId { get; set; }
        public string Rut { get; set; }
        public string Nombres { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public long Telefono { get; set; }
        public int RegionId { get; set; }
        public int ComunaId { get; set; }
        public string Calle { get; set; }
        public int? Numero { get; set; }
        public int? Depto { get; set; }
        public int TipoEntregaId { get; set; }
        public DateTime FechaCreacion { get; set; }
        public bool Vigente { get; set; }
    }
}
