﻿using System;
using Car1.Flujo.Compra.Api.Entity.FlujoCompra;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace Car1.Flujo.Compra.Api.Infrastructure.Data
{
    public partial class FlujoCompraContext : DbContext
    {
        public FlujoCompraContext()
        {
        }

        public FlujoCompraContext(DbContextOptions<FlujoCompraContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TAbono> TAbonos { get; set; }
        public virtual DbSet<TAbonoEstado> TAbonoEstados { get; set; }
        public virtual DbSet<TArticulo> TArticulos { get; set; }
        public virtual DbSet<TArticuloAtributo> TArticuloAtributos { get; set; }
        public virtual DbSet<TArticuloTipo> TArticuloTipos { get; set; }
        public virtual DbSet<TAtributo> TAtributos { get; set; }
        public virtual DbSet<TEntrega> TEntregas { get; set; }
        public virtual DbSet<TEtapaFlujo> TEtapaFlujos { get; set; }
        public virtual DbSet<TFlujoCompra> TFlujoCompras { get; set; }
        public virtual DbSet<TFlujoTiempo> TFlujoTiempos { get; set; }
        public virtual DbSet<TPersonaFlujo> TPersonaFlujos { get; set; }
        public virtual DbSet<TTipoDato> TTipoDatos { get; set; }
        public virtual DbSet<TTipoEntrega> TTipoEntregas { get; set; }
        public virtual DbSet<TFlujoEstado> TFlujoEstados { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseMySQL(Util.Constants.AppSettings.ConnectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TAbono>(entity =>
            {
                entity.HasKey(e => e.AbonoId)
                    .HasName("PRIMARY");

                entity.ToTable("T_Abono");

                entity.HasIndex(e => e.AbonoEstadoId, "AbonoEstadoId_idx");

                entity.Property(e => e.OrdenKlap).HasMaxLength(200);

                entity.HasOne(d => d.AbonoEstado)
                    .WithMany(p => p.TAbonos)
                    .HasForeignKey(d => d.AbonoEstadoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("AbonoEstadoId");
            });

            modelBuilder.Entity<TAbonoEstado>(entity =>
            {
                entity.HasKey(e => e.AbonoEstadoId)
                    .HasName("PRIMARY");

                entity.ToTable("T_Abono_Estado");

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Vigente).HasColumnType("bit(1)");
            });

            modelBuilder.Entity<TArticulo>(entity =>
            {
                entity.HasKey(e => e.ArticuloId)
                    .HasName("PRIMARY");

                entity.ToTable("T_Articulo");

                entity.HasIndex(e => e.ArticuloTipoId, "ArticuloTipoId_idx");

                entity.Property(e => e.IdentificadorExterno)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(300);

                entity.Property(e => e.Vigente).HasColumnType("bit(1)");

                entity.HasOne(d => d.ArticuloTipo)
                    .WithMany(p => p.TArticulos)
                    .HasForeignKey(d => d.ArticuloTipoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ArticuloTipoId");
            });

            modelBuilder.Entity<TArticuloAtributo>(entity =>
            {
                entity.HasKey(e => e.ArticuloAtributoId)
                    .HasName("PRIMARY");

                entity.ToTable("T_Articulo_Atributo");

                entity.HasIndex(e => e.ArticuloId, "ArticuloId_idx");

                entity.HasIndex(e => e.AtributoId, "AtributoId_idx");

                entity.Property(e => e.Valor)
                    .IsRequired()
                    .HasMaxLength(400);

                entity.Property(e => e.Vigente).HasColumnType("bit(1)");

                entity.HasOne(d => d.Articulo)
                    .WithMany(p => p.TArticuloAtributos)
                    .HasForeignKey(d => d.ArticuloId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ArticuloId");

                entity.HasOne(d => d.Atributo)
                    .WithMany(p => p.TArticuloAtributos)
                    .HasForeignKey(d => d.AtributoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("AtributoId");
            });

            modelBuilder.Entity<TArticuloTipo>(entity =>
            {
                entity.HasKey(e => e.ArticuloTipoId)
                    .HasName("PRIMARY");

                entity.ToTable("T_Articulo_Tipo");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(400);

                entity.Property(e => e.Tipo)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.Vigente).HasColumnType("bit(1)");
            });

            modelBuilder.Entity<TAtributo>(entity =>
            {
                entity.HasKey(e => e.AtributoId)
                    .HasName("PRIMARY");

                entity.ToTable("T_Atributo");

                entity.HasIndex(e => e.TipoDatoId, "TipoDatoId_idx");

                entity.Property(e => e.Atributo)
                    .IsRequired()
                    .HasMaxLength(300);

                entity.Property(e => e.Vigente).HasColumnType("bit(1)");

                entity.HasOne(d => d.TipoDato)
                    .WithMany(p => p.TAtributos)
                    .HasForeignKey(d => d.TipoDatoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("TipoDatoId");
            });

            modelBuilder.Entity<TEntrega>(entity =>
            {
                entity.HasKey(e => e.EntregaId)
                    .HasName("PRIMARY");

                entity.ToTable("T_Entrega");

                entity.HasIndex(e => e.EntregaId, "EntregaId_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.Nombres)
                    .IsRequired()
                    .HasMaxLength(70);

                entity.Property(e => e.PrimerApellido)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Rut)
                    .IsRequired()
                    .HasMaxLength(14);

                entity.Property(e => e.SegundoApellido)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Vigente).HasColumnType("bit(1)");
            });

            modelBuilder.Entity<TEtapaFlujo>(entity =>
            {
                entity.HasKey(e => e.EtapaId)
                    .HasName("PRIMARY");

                entity.ToTable("T_Etapa_Flujo");

                entity.Property(e => e.Etapa)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.Vigente).HasColumnType("bit(1)");
            });

            modelBuilder.Entity<TFlujoCompra>(entity =>
            {
                entity.HasKey(e => e.FlujoCompraId)
                    .HasName("PRIMARY");

                entity.ToTable("T_Flujo_Compra");

                entity.HasComment("		");

                entity.HasIndex(e => e.PersonaId, "PersonaFlujoIdName_idx");

                entity.HasIndex(e => e.PerTransferenciaId, "PersonaFlujoTerceroIdName_idx");

                entity.Property(e => e.SessionId)
                    .IsRequired()
                    .HasMaxLength(300);

                entity.Property(e => e.TransferenciaArticulo).HasColumnType("decimal(10,0)");

                entity.Property(e => e.ValorArticulo).HasColumnType("decimal(10,0)");

                entity.Property(e => e.ValorEntrega).HasColumnType("decimal(10,0)");

                entity.Property(e => e.Vigente).HasColumnType("bit(1)");

                entity.HasOne(d => d.PerTransferencia)
                    .WithMany(p => p.TFlujoCompraPerTransferencia)
                    .HasForeignKey(d => d.PerTransferenciaId)
                    .HasConstraintName("PersonaFlujoTerceroIdName");

                entity.HasOne(d => d.Persona)
                    .WithMany(p => p.TFlujoCompraPersonas)
                    .HasForeignKey(d => d.PersonaId)
                    .HasConstraintName("PersonaFlujoIdName");
            });

            modelBuilder.Entity<TFlujoEstado>(entity =>
            {
                entity.HasKey(e => e.EstadoId)
                    .HasName("PRIMARY");

                entity.ToTable("T_Flujo_Estado");

                entity.HasIndex(e => e.EstadoId, "EstadoId_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(45);
            });

            modelBuilder.Entity<TFlujoTiempo>(entity =>
            {
                entity.HasKey(e => e.TiempoId)
                    .HasName("PRIMARY");

                entity.ToTable("T_Flujo_Tiempo");

                entity.HasIndex(e => e.FlujoCompraId, "FlujoCompraId_idx");

                entity.Property(e => e.Vigente).HasColumnType("bit(1)");

                entity.HasOne(d => d.FlujoCompra)
                    .WithMany(p => p.TFlujoTiempos)
                    .HasForeignKey(d => d.FlujoCompraId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FlujoCompraId");
            });

            modelBuilder.Entity<TPersonaFlujo>(entity =>
            {
                entity.HasKey(e => e.PersonaFlujoId)
                    .HasName("PRIMARY");

                entity.ToTable("T_Persona_Flujo");

                entity.Property(e => e.Calle)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.Property(e => e.EsEmpresa).HasColumnType("bit(1)");

                entity.Property(e => e.Giro).HasMaxLength(400);

                entity.Property(e => e.Nombres)
                    .IsRequired()
                    .HasMaxLength(70);

                entity.Property(e => e.PrimerApellido)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.RazonSocial).HasMaxLength(200);

                entity.Property(e => e.Rut)
                    .IsRequired()
                    .HasMaxLength(14);

                entity.Property(e => e.SegundoApellido)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TTipoDato>(entity =>
            {
                entity.HasKey(e => e.TipoDatoId)
                    .HasName("PRIMARY");

                entity.ToTable("T_Tipo_Dato");

                entity.HasComment("	");

                entity.Property(e => e.TipoDato)
                    .IsRequired()
                    .HasMaxLength(300);

                entity.Property(e => e.Vigente).HasColumnType("bit(1)");
            });

            modelBuilder.Entity<TTipoEntrega>(entity =>
            {
                entity.HasKey(e => e.TipoEntregaId)
                    .HasName("PRIMARY");

                entity.ToTable("T_Tipo_Entrega");

                entity.HasIndex(e => e.TipoEntregaId, "TipoEntregaId_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(70);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
