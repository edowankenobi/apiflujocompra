﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car1.Flujo.Compra.Api.Model.Service
{
    public class ArticuloServiceModel
    {
        [JsonProperty("articuloId")]
        public int ArticuloId { get; set; }
        [JsonProperty("precio")]
        public decimal Precio { get; set; }
        [JsonProperty("valorTransferencia")]
        public decimal ValorTransferencia { get; set; }
    }
}
