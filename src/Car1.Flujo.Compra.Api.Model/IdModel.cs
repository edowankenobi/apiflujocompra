﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Car1.Flujo.Compra.Api.Model
{
    public class IdModel<T>
    {
        public IdModel() { }
        public IdModel(T value)
        {
            Value = value;
        }
        public T Value { get; set; }
    }
}
