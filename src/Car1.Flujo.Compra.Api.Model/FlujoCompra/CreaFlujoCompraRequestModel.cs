﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Car1.Flujo.Compra.Api.Model.FlujoCompra
{
    public class CreaFlujoCompraRequestModel
    {
        public string Patente { get; set; }
        public string SessionId { get; set; }
    }
}
