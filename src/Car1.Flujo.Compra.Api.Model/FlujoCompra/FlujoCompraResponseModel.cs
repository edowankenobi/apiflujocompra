﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Car1.Flujo.Compra.Api.Model.FlujoCompra
{
    public class FlujoCompraResponseModel
    {
        public int FlujoCompraId { get; set; }
        public int ArticuloId { get; set; }
        public int? PersonaId { get; set; }
        public int? PerTransferenciaId { get; set; }
        public decimal ValorArticulo { get; set; }
        public decimal TransferenciaArticulo { get; set; }
        public decimal ValorEntrega { get; set; }
        public int? FinPersonaId { get; set; }
    }
}
