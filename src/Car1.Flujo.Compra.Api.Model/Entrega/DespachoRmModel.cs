﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Car1.Flujo.Compra.Api.Model.Entrega
{
    public class DespachoRmModel
    {
        [Required(ErrorMessage = "Campo obligatorio")]
        public string Rut { get; set; }

        [Required(ErrorMessage = "Campo obligatorio")]
        [MaxLength(70, ErrorMessage = "Largo máximo {1} caracteres")]
        public string Nombres { get; set; }

        [Required(ErrorMessage = "Campo obligatorio")]
        [MaxLength(50, ErrorMessage = "Largo máximo {1} caracteres")]
        public string PrimerApellido { get; set; }

        [Required(ErrorMessage = "Campo obligatorio")]
        [MaxLength(50, ErrorMessage = "Largo máximo {1} caracteres")]
        public string SegundoApellido { get; set; }

        [Required(ErrorMessage = "Campo obligatorio")]
        public string Telefono { get; set; }

        //[Required(ErrorMessage = "Campo obligatorio")]
        //public int Region { get; set; }

        [Required(ErrorMessage = "Campo obligatorio")]
        public int Comuna { get; set; }

        [Required(ErrorMessage = "Campo obligatorio")]
        [MaxLength(500, ErrorMessage = "Largo máximo {1} caracteres")]
        public string Calle { get; set; }

        [Required(ErrorMessage = "Campo obligatorio")]
        [Range(1, 9999999, ErrorMessage = "Campo debe ser un número válido")]
        public string Numero { get; set; }

        [Range(1, 99999, ErrorMessage = "Campo debe ser un número válido")]
        public string Depto { get; set; }

        [Required]
        public int FlujoId { get; set; }
    }
}
