﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Car1.Flujo.Compra.Api.Model
{
    public class IdValue<T1, T2>
    {
        public IdValue()
        {
        }
        public IdValue(T1 id, T2 value)
        {
            Id = id;
            Value = value;
        }
        public T1 Id { get; set; }
        public T2 Value { get; set; }
    }
}
