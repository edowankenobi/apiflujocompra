﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Car1.Flujo.Compra.Api.Model.PersonaFlujo
{
    public class PersonaModelResponse
    {
        public string Rut { get; set; }
        public string Nombres { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }
        public int Region { get; set; }
        public int Comuna { get; set; }
        public string Calle { get; set; }
        public string Numero { get; set; }
        public string Depto { get; set; }
    }
}
