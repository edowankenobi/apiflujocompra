﻿using Car1.Flujo.Compra.Api.Model.Service;
using Car1.Flujo.Compra.Api.Util;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Threading.Tasks;

namespace Car1.Flujo.Compra.Api.Services
{
    public class ArticuloService
    {
        public async Task<ArticuloServiceModel> CrearArticulo(string patente)
        {
            var client = new RestClient($"{Constants.AppSettings.BaseArticulo}{Constants.AppSettings.AgregarArticulo}");

            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");

            var body = @"{""Valor"": """+ patente + @"""}";
            request.AddParameter("application/json", body, ParameterType.RequestBody);

            IRestResponse response = await client.ExecuteAsync<ArticuloServiceModel>(request);
            ArticuloServiceModel respuesta = JsonConvert.DeserializeObject<ArticuloServiceModel>(response.Content);

            return respuesta;
        }
    }
}
